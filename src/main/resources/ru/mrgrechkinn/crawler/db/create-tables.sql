CREATE TABLE IF NOT EXISTS tbl_gz_ooo (
    id IDENTITY PRIMARY KEY,
    participient_id INT NOT NULL,
    is_downloaded BOOLEAN DEFAULT FALSE
);