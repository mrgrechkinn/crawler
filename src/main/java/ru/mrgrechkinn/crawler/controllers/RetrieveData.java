package ru.mrgrechkinn.crawler.controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.Proxy.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.IOUtils;

public class RetrieveData implements Runnable {

    private static final String url = "http://gzakupki.ru/guide/supplierdet.aspx?supplierInn=";

    private List<String> ids = new ArrayList<String>();
    private UserInfoParser parser = new UserInfoParser();
    private String uuid;

    public RetrieveData() {
        uuid = UUID.randomUUID().toString();
    }

    @Override
    public void run() {
        File file = new File("D:\\lala2\\" + uuid + ".sql");
        PrintWriter flt = null;
        try {
            flt = new PrintWriter(file);
            for (String id : ids) {
                try {
                    HttpURLConnection connection = (HttpURLConnection) new URL(url + id).openConnection();//new Proxy(Type.HTTP, new InetSocketAddress("localhost", 8888)));
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0");
                    connection.setConnectTimeout(10000);
                    //connection.setRequestProperty("Content-Length", "9999");
                    connection.setRequestProperty("Content-Type", "text/html; charset=utf-8");
                    connection.connect();
                    parser.parseHtml((IOUtils.toString(connection.getInputStream())));
                    flt.println(parser.toString());
                    connection.disconnect();
                } catch (Exception e) {
                    System.out.println("Error from retrieve data" + e.getMessage());
                }
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } finally {
            if (flt != null) {
                flt.close();
            }
        }

    }

    public void addId(String id) {
        ids.add(id);
    }

}
