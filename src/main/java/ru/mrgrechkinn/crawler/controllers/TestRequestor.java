package ru.mrgrechkinn.crawler.controllers;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import ru.mrgrechkinn.crawler.proxy.ProxyManager;

import com.google.inject.Inject;

public class TestRequestor {

    private static int id = 0;

    public static String E_TAR = "__EVENTTARGET";
    public static String E_ARG = "__EVENTARGUMENT";
    public static String E_VAL = "__EVENTVALIDATION";
    public static String V_ST = "__VIEWSTATE";
    public static String NAV_BAR = "ctl00_Navigation_NavigationBarGS";
    public static String LOGIN = "ctl00$Login$txtLogin";
    public static String PASS = "ctl00$Login$txtPassword";
    public static String ORG_RAW = "ctl00_ContentHolder_txtOrganizationName_Raw";
    public static String ORG_NAME = "ctl00$ContentHolder$txtOrganizationName";
    public static String CHK_CONFORM = "ctl00$ContentHolder$chkConform";
    public static String DXScript = "DXScript";
    public static String Callback = "ctl00$ContentHolder$grdSupplier$CallbackState";
    public static String Callback_ID= "__CALLBACKID";
    public static String Callback_PARAM = "__CALLBACKPARAM";

    private URL url;
    private boolean proxyEnabled = false;

    @Inject
    private ProxyManager proxyManager;

    private Map<String, String> params = new HashMap<String, String>();

    public void firstRequest() throws IOException {
        HttpURLConnection connection;
        if (proxyEnabled) {
            connection = (HttpURLConnection) url.openConnection(proxyManager.getProxy());
        } else {
            connection = (HttpURLConnection) url.openConnection();
        }
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Length", "9999");
        connection.setRequestProperty("Content-Type", "text/html; charset=utf-8");
        connection.connect();
        parse(IOUtils.toString(connection.getInputStream()));
        connection.disconnect();
    }

    public void secondRequest() {
        try {
            firstRequest();
            HttpURLConnection connection;
            if (proxyEnabled) {
                connection = (HttpURLConnection) url.openConnection(proxyManager.getProxy());
            } else {
                connection = (HttpURLConnection) url.openConnection(new Proxy(Type.HTTP, new InetSocketAddress("localhost", 8888)));
            }
            String paramsValue = initParam();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");
            connection.setRequestProperty("Content-Length", "" + paramsValue.getBytes().length);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            DataOutputStream wf = new DataOutputStream(connection.getOutputStream());
            wf.writeBytes(paramsValue);
            wf.flush();
            wf.close();
            parseSecond(IOUtils.toString(connection.getInputStream()));
            connection.disconnect();
        } catch (IOException e) {
            System.out.println("ERROROEOREORO");
        }
        nextRequest();
    }

    public void nextRequest() {
        for (int i = 0; i < 4859; i++) {
            try {
                HttpURLConnection connection;
                if (proxyEnabled) {
                    connection = (HttpURLConnection) url.openConnection(proxyManager.getProxy());
                } else {
                    connection = (HttpURLConnection) url.openConnection(new Proxy(Type.HTTP, new InetSocketAddress("localhost", 8888)));
                }
                String paramsValue = initParam();
                connection.setRequestProperty("User-Agent", "Mozilla/5.0");
                connection.setRequestProperty("Content-Length", "" + paramsValue.getBytes().length);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                DataOutputStream wf = new DataOutputStream(connection.getOutputStream());
                wf.writeBytes(paramsValue);
                wf.flush();
                wf.close();
                String res = IOUtils.toString(connection.getInputStream());
                res = res.substring(20, res.length() - 10);
                connection.disconnect();
                parseNext(res);
                /*try {
                    JSONObject object = (JSONObject) new JSONParser().parse(res);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
            } catch (IOException e) {
                System.out.println("ERROROEOREORO");
            }
        }
    }
    /**
     * @return the proxyEnabled
     */
    public boolean isProxyEnabled() {
        return proxyEnabled;
    }

    /**
     * @param proxyEnabled the proxyEnabled to set
     */
    public void setProxyEnabled(boolean proxyEnabled) {
        this.proxyEnabled = proxyEnabled;
    }

    public void addParameter(String key, String value) {
        params.put(key, value);
    }

    public void setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    public Document parse(String html) {
        Document doc = Jsoup.parse(html);
        params.put(E_VAL, doc.getElementById(E_VAL).val());
        params.put(CHK_CONFORM, "U");
        params.put(Callback, doc.getElementById("ctl00_ContentHolder_grdSupplier_CallbackState").val());
        params.put(V_ST, doc.getElementById(V_ST).val());
        return doc;
    }

    public void parseSecond(String html) {
        Document doc = parse(html);
        params.put("__CALLBACKPARAM", "c0:KV|2;[];GB|20;12|PAGERONCLICK3|PBN;");
        params.put("__CALLBACKID", "ctl00$ContentHolder$grdSupplier");
        printId(doc);
    }

    public void parseNext(String html) {
        Document doc = Jsoup.parse(html);
        params.put(Callback, doc.getElementById("ctl00_ContentHolder_grdSupplier_CallbackState").val());
        printId(doc);
    }

    public void printId(Document doc) {
        for (int i = 0; i < 10; i++) {
            System.out.print(doc.getElementById("ctl00_ContentHolder_grdSupplier_cell" + id++ + "_0_ASPxHyperLink1").text() + ",");
        }
    }

    public String initParam() throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();
        //builder.append("ctl00$Login$txtPassword").append("=").append("&");
        //builder.append("ctl00$Login$txtLogin").append("=").append("&");
        //builder.append("ctl00$ContentHolder$btnRefresh").append("=").append("&");
        //builder.append("ctl00$ContentHolder$grdSupplier$DXSelInput").append("=").append("&");
        //builder.append("ctl00$ContentHolder$btnRefresh").append("=").append("&");
        //builder.append(E_TAR).append("=").append("&");
        //builder.append(E_ARG).append("=").append("&");
        if (params.containsKey("__CALLBACKPARAM")) {
            builder.append(URLEncoder.encode("__CALLBACKPARAM", "UTF-8")).append("=").append(URLEncoder.encode(params.get("__CALLBACKPARAM"), "UTF-8")).append("&");
        }
        if (params.containsKey("__CALLBACKID")) {
            builder.append(URLEncoder.encode("__CALLBACKID", "UTF-8")).append("=").append(URLEncoder.encode(params.get("__CALLBACKID"), "UTF-8")).append("&");
        }

        builder.append(URLEncoder.encode(V_ST, "UTF-8")).append("=").append(URLEncoder.encode(params.get(V_ST), "UTF-8")).append("&");
        builder.append(URLEncoder.encode(E_VAL, "UTF-8")).append("=").append(URLEncoder.encode(params.get(E_VAL), "UTF-8")).append("&");
        builder.append(URLEncoder.encode(NAV_BAR, "UTF-8")).append("=").append(URLEncoder.encode("0;0;0;0;0;0;1;0;0;0;0;0;0;0;0", "UTF-8")).append("&");
        builder.append(URLEncoder.encode(ORG_RAW, "UTF-8")).append("=").append(URLEncoder.encode("ООО ", "UTF-8")).append("&");
        builder.append(URLEncoder.encode(ORG_NAME, "UTF-8")).append("=").append(URLEncoder.encode("ООО ", "UTF-8")).append("&");
        builder.append(URLEncoder.encode(CHK_CONFORM, "UTF-8")).append("=").append(URLEncoder.encode("U", "UTF-8")).append("&");
        builder.append(URLEncoder.encode(Callback, "UTF-8")).append("=").append(URLEncoder.encode(params.get(Callback), "UTF-8")).append("&");
        builder.append(URLEncoder.encode(DXScript, "UTF-8")).append("=").append(URLEncoder.encode("1_42,1_75,1_60,2_34,2_41,2_27,1_52,1_66,3_7,2_30,1_41", "UTF-8")).append("&");
        builder.append("ctl00$ContentHolder$grdSupplier$DXKVInput").append("=").append("[]");
        return builder.toString();
    }
}
