package ru.mrgrechkinn.crawler.controllers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class UserInfoParser {

    public static final String typeElementId = "ctl00_ContentHolder_ASPxPageControl1_lblOrganizationForm";
    public static final String nameElementId = "ctl00_ContentHolder_ASPxPageControl1_lblOrganizationName";
    public static final String regAddElementId = "ctl00_ContentHolder_ASPxPageControl1_lblFactualAddress";
    public static final String addElementId = "ctl00_ContentHolder_ASPxPageControl1_lblPostAddress";
    public static final String innElementId = "ctl00_ContentHolder_ASPxPageControl1_lblInn";
    public static final String kppElementId = "ctl00_ContentHolder_ASPxPageControl1_lblKpp";
    public static final String emailElementId = "ctl00_ContentHolder_ASPxPageControl1_lblContactEMail";
    public static final String phoneElementId = "ctl00_ContentHolder_ASPxPageControl1_lblContactPhone";
    public static final String faxElementId = "ctl00_ContentHolder_ASPxPageControl1_lblContactFax";

    private String orgType;
    private String fullName;
    private String registeredAddress;
    private String address;
    private String inn;
    private String kpp;
    private String email;
    private String phone;
    private String fax;

    public void parseHtml(String html) {
        cleanOldData();
        try {
            Document document = Jsoup.parse(html);
            orgType = document.getElementById(typeElementId).text().replace("'", "\"");
            fullName = document.getElementById(nameElementId).text().replace("'", "\"");
            registeredAddress = document.getElementById(regAddElementId).text().replace("'", "\"");
            address = document.getElementById(addElementId).text().replace("'", "\"");
            inn = document.getElementById(innElementId).text().replace("'", "\"");
            kpp = document.getElementById(kppElementId).text().replace("'", "\"");
            email = document.getElementById(emailElementId).text().replace("'", "\"");
            phone = document.getElementById(phoneElementId).text().replace("'", "\"");
            fax = document.getElementById(faxElementId).text().replace("'", "\"");
        } catch (Exception e) {
            throw new RuntimeException("Can't parse html data", e);
        }
    }

    /**
     * @return the orgType
     */
    public String getOrgType() {
        return orgType;
    }
    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }
    /**
     * @return the registeredAddress
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }
    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }
    /**
     * @return the inn
     */
    public String getInn() {
        return inn;
    }
    /**
     * @return the kpp
     */
    public String getKpp() {
        return kpp;
    }
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }
    /**
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("insert into ip_users values (");
        builder.append("'").append(orgType).append("'").append(",");
        builder.append("'").append(fullName).append("'").append(",");
        builder.append("'").append(registeredAddress).append("'").append(",");
        builder.append("'").append(address).append("'").append(",");
        builder.append("'").append(inn).append("'").append(",");
        builder.append("'").append(kpp).append("'").append(",");
        builder.append("'").append(email).append("'").append(",");
        builder.append("'").append(phone).append("'").append(",");
        builder.append("'").append(fax).append("'").append(");");
        return builder.toString();
    }

    private void cleanOldData() {
        orgType = null;
        fullName = null;
        registeredAddress = null;
        address = null;
        inn = null;
        kpp = null;
        email = null;
        phone = null;
        fax = null;
    }

}
