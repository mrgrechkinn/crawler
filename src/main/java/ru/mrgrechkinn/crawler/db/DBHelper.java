package ru.mrgrechkinn.crawler.db;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Eugene Rudenko
 */
public interface DBHelper {

    Connection getConnection() throws SQLException;

    void closeConnection() throws SQLException;

}
