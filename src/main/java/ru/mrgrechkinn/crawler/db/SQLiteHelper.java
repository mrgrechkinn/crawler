package ru.mrgrechkinn.crawler.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.io.IOUtils;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author Eugene Rudenko
 */
public class SQLiteHelper implements DBHelper {

    private String initSQL;
    private String dbName;
    private Connection connection;

    @Inject
    private SQLiteHelper(@Named("dbName") String dbName, @Named("initSQL") String initSQL) throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        this.initSQL = initSQL;
        this.dbName = dbName;
        init();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            connection.setAutoCommit(true);
        }
        return connection;
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public void closeConnection() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    private void init() throws SQLException {
        if (initSQL != null) {
            try {
                InputStream scriptStream = new FileInputStream(getClass().getClassLoader().getResource(initSQL).getFile());
                Statement st = getConnection().createStatement();
                st.execute(IOUtils.toString(scriptStream, "UTF-8"));
            } catch(IOException e) {
                throw new RuntimeException(e);
            } finally {
                closeConnection();
            }
        }
    }

}
