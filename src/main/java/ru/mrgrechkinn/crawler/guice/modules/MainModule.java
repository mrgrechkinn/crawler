package ru.mrgrechkinn.crawler.guice.modules;

import ru.mrgrechkinn.crawler.controllers.TestRequestor;
import ru.mrgrechkinn.crawler.proxy.ProxyManager;
import ru.mrgrechkinn.crawler.service.DBServiceForGZ;

import com.google.inject.AbstractModule;

/**
 * @author Eugene Rudenko
 */
public class MainModule extends AbstractModule {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure() {
        install(new DBModule());
        bind(ProxyManager.class);
        bind(TestRequestor.class);
        bind(DBServiceForGZ.class);
        //bind(TestRequestor.class).toInstance(new TestRequestor());
    }

}
