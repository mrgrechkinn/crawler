package ru.mrgrechkinn.crawler.guice.modules;

import ru.mrgrechkinn.crawler.db.DBHelper;
import ru.mrgrechkinn.crawler.db.SQLiteHelper;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.name.Names;

/**
 * @author Eugene Rudenko
 */
public class DBModule extends AbstractModule {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void configure() {
        bindConstant().annotatedWith(Names.named("dbName")).to("crawler.db");
        bindConstant().annotatedWith(Names.named("initSQL")).to("ru/mrgrechkinn/crawler/db/create-tables.sql");
        bind(DBHelper.class).to(SQLiteHelper.class).in(Scopes.SINGLETON);
    }

}
