package ru.mrgrechkinn.crawler.service;

import java.sql.SQLException;

import ru.mrgrechkinn.crawler.db.DBHelper;

import com.google.inject.Inject;

public class DBServiceForGZ {

    private String ipSql = "INSERT INTO tbl_gz_ip (participient_id) VALUES(";
    private String oooSql = "INSERT INTO tbl_gz_ooo (participient_id) VALUES (";
    @Inject
    private DBHelper helper;

    public void save(String participientId, String type) throws SQLException {
        helper.getConnection().createStatement().execute((type.equals("ip") ? ipSql : oooSql) + "'" + participientId + "')");
    }

}
