package ru.mrgrechkinn.crawler.proxy;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.google.inject.Inject;

public class ProxyManager {

    private static int index = 0;
    private List<String> proxyList = new ArrayList<String>();
    private Proxy proxy;

    public void init() throws IOException {
        InputStream scriptStream = new FileInputStream(getClass().getClassLoader().getResource("proxy2.txt").getFile());
        proxyList = Arrays.asList(IOUtils.toString(scriptStream, "UTF-8").split(","));
    }

    public Proxy getNewProxy() {
        if (proxyList.size() > 0 && index < proxyList.size()) {
            String proxyString = proxyList.get(index++);
            String[] details = proxyString.split(":");
            Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(details[0], Integer.parseInt(details[1])));
            this.proxy = proxy;
            return proxy;
        } else {
            System.out.println("Erro when init proxy Manager");
            return null;
        }
    }

    public Proxy getProxy() {
        if (proxy == null) {
            proxy = getNewProxy();
        }
        return proxy;
    }

    @Inject
    private ProxyManager() {
        try {
            init();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
